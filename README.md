# GitLab Runner Stack

This CloudFormation stack creates an AutoScaling group which launches (at least one) EC2
instance that then runs the GitLab pipeline build Runner as a Docker container.

## Setup

To use this stack, you will first have to know your GitLab Runner registration token; you
can find this in the GitLab UI once you've logged in by going to one of your projects (yes,
you'll have to create or fork one first) and then:

1. Click the "gear wheel" settings button in the top-right of the UI, and select "Runners"
   from the drop-down menu which appears
1. Under "Specific Runners", there are setup instructions which include a Registration
   Token. Copy that and edit it into the params file.

## Creating the runner

1. Create the cloudformation stack in your Normal Way. To start with, only Sydney and Oregon
   regions are supported.
